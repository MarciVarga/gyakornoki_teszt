<?php

namespace AppBundle\Command;

use AppBundle\Entity\Wallpaper;
use Doctrine\DBAL\Schema\Table;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetupWallpapersCommand extends Command
{
    /**
     * SetupWallpapersCommand constructor.
     * @param string $rootDir
     */
    public function __construct(string $rootDir, EntityManagerInterface $em)
    {
        parent::__construct();

        $this->rootDir = $rootDir;
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setName('app:setup-wallpapers')
            ->setDescription('Wallpaper entity create')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $i = 0;

        $wallpapers = glob($this->rootDir.'/../web/images/*.*');

        $fileNames = [];

        foreach ($wallpapers as $wallpaper) {

            $wp = (new Wallpaper())
                ->setFilename($wallpaper)
                ->setPosition($i)
                ->setUserid()
                ->setName('vmi')
                ->setWidth(1920)
                ->setHeight(1080);

            $i += $i+1;

            $this->em->persist($wp);

            $fileNames[] = [$fileNames];
        }

        $this->em->flush();

        $output->writeln('Command result.');
    }

}
