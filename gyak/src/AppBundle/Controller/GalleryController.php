<?php


namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Wallpaper;

class GalleryController extends Controller
{
    /**
     * @Route("/gallery", name="gallery")
     */
    public function indexAction(Request $request) {

        $wallpapers = $this->getDoctrine()->getRepository('AppBundle:Wallpaper')->findAll();
        return $this->render('gallery/index.html.twig', [
            'wallpapers' => $wallpapers
        ]);
    }

    /**
     * @Route("/gallery/view/{id}", name="view")
     */
    public function viewAction($id) {
        $wallpapers = $this->getDoctrine()->getRepository('AppBundle:Wallpaper')->findAll();
        return $this->render('gallery/view.html.twig', [
            'wallpapers' => $wallpapers,
            'wId' => $id
        ]);
    }

    /**
     * @Route("/gallery/edit/{id}", name="edit")
     */
    public function editAction($id) {

        return $this->render('ed/edit.html.twig');
    }

    /**
     * @Route("/gallery/delete/{id}", name="delete")
     */
    public function deleteAction($id) {

        return $this->render('ed/delete.html.twig');
    }
}