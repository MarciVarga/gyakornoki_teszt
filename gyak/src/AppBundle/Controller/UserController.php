<?php

namespace AppBundle\Controller;

use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Model\UserModel;

class UserController extends Controller
{
    /**
     * @Route("/user")
     */
    public function showAction() {

        $a = [['szemely1','?'],['szemely2','??']];
        $b = count($a)-1;

        return $html = $this->render('user/show.html.twig', [
            'userArray' => $a,
            'length' => $b
        ]);

    }


}