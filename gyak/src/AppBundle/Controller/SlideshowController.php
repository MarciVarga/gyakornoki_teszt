<?php


namespace AppBundle\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Wallpaper;


class SlideshowController extends Controller
{
    /**
     * @Route("/slideshow", name="slideshow")
     */
    public function indexAction(Request $request) {

        $wallpapers = $this->getDoctrine()->getRepository('AppBundle:Wallpaper')->findAll();
        return $this->render('slideshow/index.html.twig', [
            'wallpapers' => $wallpapers
        ]);
    }
}