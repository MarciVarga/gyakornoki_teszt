<?php

use AppBundle\Entity\Wallpaper;



        $target_dir = "images/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        // Kép-e vagy sem
        if (isset($_POST["submit"])) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if ($check !== false) {
                echo "   Ez a fájl egy kép - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "   A fájl nem egy kép.";
                $uploadOk = 0;
            }
        }

        // Check file size
        if ($_FILES["fileToUpload"]["size"] > 500000) {
            echo "   Túl nagy a kép.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif") {
            echo "   Csak JPG, JPEG, PNG és GIF fájlt lehet csak feltölteni!";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "   A fájl nem lett feltöltve.";
            // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                echo "   A   fájl " . basename($_FILES["fileToUpload"]["name"]) . " feltöltve";
                list($width, $height) = getimagesize($_FILES["fileToUpload"]["tmp_name"]);


                $image = new Wallpaper();
                $image->setFilename($_FILES["fileToUpload"]["name"]);
                $image->setPosition(2);
                $image->setWidth(1920);
                $image->setHeight(1080);

                // Létezik-e már
                if (file_exists($target_file) && $_POST["wpname"] != null) {
                    $neve = $_POST["wpname"];
                    $neve = $neve . (1);
                    $image->setName($neve);
                } elseif ($_POST["wpname"] == null) {
                    $image->setName($_FILES["fileToUpload"]["name"]);
                } else {
                    $image->setName($_POST["wpname"]);
                }

            } else {
                echo "   Hiba történt feltöltés közben.";
            }
        }